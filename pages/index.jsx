import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';

export default function Home() {
    return (
        <div className="bg-gray-100">
            <Head>
                <meta charset="utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />

                <title>My Blog</title>
            </Head>
            <main>
                <div className="text-center p-8">
                    <h1 className="uppercase text-2xl">my blog</h1>
                    <p>
                        Welcome to the blog of <span className="text-white bg-black">unknown</span>
                    </p>
                </div>
                <section className="">
                    <section className="grid grid-col-10  gap-2 pb-16">
                        <div className="lg:col-start-3 lg:col-end-5 col-start-1 col-end-7 gap-2 p-4">
                            <section className="border-gray-50 shadow-2xl bg-white ">
                                <img className="w-full" src="/image/woods.jpg" alt="woods" />
                                <div className="mb-4 p-2">
                                    <h3 className="font-black text-xl m-2 mb-4">TITLE HEADING</h3>
                                    <p className="m-2 mb-4">
                                        Title description,
                                        <span className="text-gray-400"> April 7, 2014 </span>
                                    </p>
                                    <p className="m-2 mb-4">
                                        Mauris neque quam, fermentum ut nisl vitae, convallis
                                        maximus nisl. Sed mattis nunc id lorem euismod placerat.
                                        Vivamus porttitor magna enim, ac accumsan tortor cursus at.
                                        Phasellus sed ultricies mi non congue ullam corper. Praesent
                                        tincidunt sed tellus ut rutrum. Sed vitae justo condimentum,
                                        porta lectus vitae, ultricies congue gravida diam non
                                        fringilla.
                                    </p>
                                    <div className="grid grid-col-6 gap-4 m-2">
                                        <div className="col-start-1 col-end-3">
                                            <button className="btn bg-transparent hover:bg-gray-500 text-gray-700 font-semibold hover:text-white py-2 px-4 border border-gray-500 hover:border-transparent rounded uppercase mb-2">
                                                read more {'>>'}
                                            </button>
                                        </div>
                                        <div className="col-end-7 ">
                                            <p className="capitalize mt-2">
                                                comments{' '}
                                                <span className="text-white bg-black p-1">0</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="border-gray-50 shadow-2xl bg-white ">
                                <img className="w-full" src="/image/bridge.jpg" alt="bridge" />
                                <div className="m-3">
                                    <h3 className="font-black text-xl mb-4">BLOG ENTRY</h3>
                                    <p className=" mb-4">
                                        Title description,
                                        <span className="text-gray-400"> April 2, 2014 </span>
                                    </p>
                                    <p className="mb-4">
                                        Mauris neque quam, fermentum ut nisl vitae, convallis
                                        maximus nisl. Sed mattis nunc id lorem euismod placerat.
                                        Vivamus porttitor magna enim, ac accumsan tortor cursus at.
                                        Phasellus sed ultricies mi non congue ullam corper. Praesent
                                        tincidunt sed tellus ut rutrum. Sed vitae justo condimentum,
                                        porta lectus vitae, ultricies congue gravida diam non
                                        fringilla.
                                    </p>
                                    <div className="grid grid-col-6 gap-4 mb-2">
                                        <div className="col-start-1 col-end-3">
                                            <button className="btn bg-transparent hover:bg-gray-500 text-gray-700 font-semibold hover:text-white py-2 px-4 border border-gray-500 hover:border-transparent rounded uppercase mb-2">
                                                read more {'>>'}
                                            </button>
                                        </div>
                                        <div className="col-end-6 ">
                                            <p className="capitalize">
                                                comments{' '}
                                                <span className="text-white rounded-full bg-black p-1">
                                                    2
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div className="lg:col-start-7 lg:col-end-8  col-start-1 col-end-7 gap-2 p-4">
                            <section className="border-gray-50 shadow-2xl bg-white">
                                <img className="w-full" src="image/avatar_g.jpg" alt="avatar" />
                                <div className=" p-2">
                                    <h3 className="font-black">MY NAME</h3>
                                    <p className="mt-4">
                                        Just me, myself and I, exploring the universe of uknownment.
                                        I have a heart of love and a interest of lorem ipsum and
                                        mauris neque quam blog. I want to share my world with you.
                                    </p>
                                </div>
                            </section>
                            <section className="border-gray-50 shadow-2xl bg-white mt-4">

                                <div className="grid grid-rows-4">
                                    <div className="row-end-1 row-span-1 p-3 text-xl bg-gray-100">
                                        Popular Posts
                                    </div>

                                    <div className="row-span-1 grid grid-cols-4">
                                        <div className="col-end-1">
                                            <img
                                                class="w-20 m-3"
                                                src="../image/workshop.jpg"
                                                alt="workshop"
                                            />
                                        </div>
                                        <div className="col-start-1 col-span-4 grid grid-rows-4 ">
                                            <p className="row-start-2 row-end-3 text-xl ">Lorem</p>
                                            <p className="row-start-3 row-end-4">Sed mattis nunc</p>
                                        </div>
                                    </div>
                                    <div className="row-span-1 grid grid-cols-4 border-t-2 border-gray-200">
                                        <div className="col-end-1">
                                            <img
                                                class="w-20 m-3"
                                                src="../image/gondol.jpg"
                                                alt="gondol"
                                            />
                                        </div>
                                        <div className="col-start-1 col-span-4 grid grid-rows-4 ">
                                            <p className="row-start-2 row-end-3 text-xl ">Ipsum</p>
                                            <p className="row-start-3 row-end-4">Praes tinci sed</p>
                                        </div>
                                    </div>
                                    <div className="row-span-1 grid grid-cols-4 border-t-2 border-gray-200">
                                        <div className="col-end-1">
                                            <img
                                                class="w-20 m-3"
                                                src="../image/skies.jpg" alt="skies"
                                            />
                                        </div>
                                        <div className="col-start-1 col-span-4 grid grid-rows-4">
                                            <p className="row-start-2 row-end-3 text-xl ">Dorum</p>
                                            <p className="row-start-3 row-end-4">Ultricies congue</p>
                                        </div>
                                    </div>
                                    <div className="row-span-1 grid grid-cols-4 border-t-2 border-gray-200">
                                        <div className="col-end-1">
                                            <img
                                                class="w-20 m-3"
                                                src="../image/rock.jpg" alt="rock"
                                            />
                                        </div>
                                        <div className="col-start-1 col-span-4 grid grid-rows-4">
                                            <p className="row-start-2 row-end-3 text-xl ">Mingsum</p>
                                            <p className="row-start-3 row-end-4">Lorem ipsum dipsum</p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="border-gray-50 shadow-2xl bg-white mt-4">
                            <div className="grid grid-rows-3">
                                    <div className="row-end-1 row-span-1 p-3 text-xl bg-gray-100">
                                    Tags
                                    </div>
                                    <div className="row-start-1 row-span-2 p-3 flex flex-wrap">
                                        <span className=" bg-black text-white p-1 text-base">Travel</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">New York</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">London</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">IKEA</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">NORWAY</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">DIY</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">Ideas</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">Baby</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">Family</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">News</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">Clothing</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">Shopping</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">Sports</span>
                                        <span className="m-1 bg-gray-200 text-black p-1 text-sm">Games</span>
                                    </div>
                                    </div>
                            </section>
                        </div>
                    </section>
                </section>
            </main>
        </div>
    );
}
